package domain;

public class DecoratorQuetesNumberTwo implements PrinterInt {

    PrinterInt component;

    public DecoratorQuetesNumberTwo(PrinterInt component) {
        this.component = component;
    }

    @Override
    public void print() {
        System.out.println("|");
        component.print();
        System.out.println("|");
    }
}
