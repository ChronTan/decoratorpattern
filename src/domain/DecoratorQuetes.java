package domain;

public class DecoratorQuetes implements PrinterInt {

    PrinterInt component;

    public DecoratorQuetes(PrinterInt component) {
        this.component = component;
    }

    @Override
    public void print() {
        System.out.println("!");
        component.print();
        System.out.println("!");
    }
}
